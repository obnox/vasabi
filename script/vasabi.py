#!/usr/bin/env python

# vasabi - Vagrant Samba Build
# Program to run samba builds in vagrant managed machines
#
# Copyright (C) Michael Adam 2015
#
# License GPLv3+ (see COPYING)

import optparse
import subprocess
import os
import inspect

default_target = "trusty64-lxc"
default_provider = "lxc"
default_src_dir_host = "."

bld_dir_guest = "/home/vagrant/vasabi-build"
src_dir_guest = "/vasabi/src"
script_dir = "/vagrant/script"
bin_dir_suffix = ""

default_build_wrapper = "%s/samba-build.py" % script_dir

options = None
args = None

orig_dir = os.getcwd()
prog_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
vasabi_basedir = os.path.abspath("%s/.." % prog_dir)

default_src_dir_host = orig_dir

# we need to have the Vagrantfile
os.chdir(vasabi_basedir)


#
# Processing of command line options.
#
def process_args():
    global options
    global args
    global bin_dir_suffix

    parser = optparse.OptionParser()

    # alternative to storing the defaults inside the add_option calls:
    #parser.set_defaults(do_configure = True,
    #                    do_make_clean = False,
    #                    do_test = False,
    #                    do_out_of_tree = True)

    parser.add_option("-t", "--target",
                      help = "build on machine TARGET (default: %s)" %
                             default_target,
                      type = "str",
                      dest = "target",
                      metavar = "TARGET",
                      default = default_target)

    #
    # TODO: Get rid of the --provider option and
    # have an inventory of machines/images/types instead.
    # This requires to make the Vagrantfile more dynamic...
    #
    parser.add_option("-p", "--provider",
                      help = "provider for target machine (default %s)" %
                             default_provider,
                      type = "str",
                      dest = "provider",
                      metavar = "PROVIDER",
                      default = default_provider)

    parser.add_option("-r", "--reload",
                      help = "reload the machine",
                      dest = "do_reload",
                      default = False,
                      action = "store_true")

    parser.add_option("-w", "--build-wrapper",
                      help = "the build wrapper program to run inside "
                             "build the machine",
                      type = "str",
                      dest = "build_wrapper",
                      default = default_build_wrapper,
                      action = "store")

    parser.add_option("-s", "--src-dir",
                      help = "the directory containing the sources to build",
                      type = "str",
                      dest = "src_dir_host",
                      default = default_src_dir_host,
                      action = "store")

    (options, args) = parser.parse_args()

    #
    # We should find a way to only interpret and read args
    # that lead to actions in this external program and
    # simply pass extra args on to the program executed
    # in the guest.
    #
    #if len(args) != 0:
    #    parser.error("too many arguments")

    bin_dir_suffix = options.target

    additional_args = [
        "--src-dir", src_dir_guest,
        "--bld-dir", bld_dir_guest,
        "--bin-dir-suffix", bin_dir_suffix,
        ]

    args.extend(additional_args)

    print "=" * 78
    print "target:         %s"  % options.target
    print "provider:       %s"  % options.provider
    print "do_reload:      %s"  % options.do_reload
    print "build_wrapper: '%s'" % options.build_wrapper
    print "src_dir:       '%s'" % options.src_dir_host
    print ""
    print "args:          '%s'" % ' '.join(args)
    print "=" * 78


def setup_target():
    """ set up the build machine """

    cmd = "vagrant status %s 2>/dev/null | grep -q running" % options.target
    print "cmd: '%s'" % cmd
    ret = subprocess.call(cmd, shell=True)

    if ret == 0 and options.do_reload:
        cmd = [ "vagrant", "halt", options.target ]
        ret = subprocess.call(cmd)
        if ret != 0:
            return ret

    cmd = [ "vagrant",
            "--src-dir-host=%s"  % options.src_dir_host,
            "--src-dir-guest=%s" % src_dir_guest,
            "up", options.target,
            "--provider=%s" % options.provider, ]
    print "cmd: '%s'" % ' '.join(cmd)
    ret = subprocess.call(cmd)

    return ret


def run_build():
    """ run the build in the machine """

    #cmd = [ "vagrant", "ssh", options.target,
    #        "--command", '"', options.build_wrapper ] + args + [ '"' ]
    #print "cmd: '%s'" % ' '.join(cmd)
    cmd = "vagrant ssh %s --command \"%s %s\"" % (
            options.target, options.build_wrapper, ' '.join(args))
    print "cmd: '%s'" % cmd
    ret = subprocess.call(cmd, shell=True)

def main():
    process_args()
    ret = setup_target()
    if ret != 0:
        return ret

    run_build()

main()

os.chdir(orig_dir)

