#!/usr/bin/env bash

BACKUP_SUFFIX="orig.$(date +%Y%m%d-%H%M%S)"

DO_BACKUP_FIRST=${DO_BACKUP_FIRST:-1}

DO_TEST=${DO_TEST:-0}
THE_TESTS="${TESTS}"

DO_CONFIGURE=${DO_CONFIGURE:-1}

DO_MAKE_CLEAN=${DO_MAKE_CLEAN:-0}
DO_MAKE=${DO_MAKE:-1}

SRC_DIR=${SRC_DIR:-"."}
BLD_DIR=${BLD_DIR:-"${SRC_DIR}"}

BIN_DIR_SUFFIX=${BIN_DIR_SUFFIX:-"$(uname -n)"}
BIN_DIR=${BIN_DIR}:-"bin.${BIN_DIR_SUFFIX}"}

SCRIPT_DIR=$(dirname $0)
BUILDNICE=${BUILDNICE:-"${SCRIPT_DIR}/buildnice"}

# re-use an existing build dir?
DO_REUSE_BINDIR=${DO_REUSE_BINDIR:-1}

DO_OUT_OF_TREE=${DO_OUT_OF_TREE:-1}

if [ "${SRC_DIR}" != "${BLD_DIR}" ]; then
  DO_OUT_OF_TREE=1
fi

if [ "${DO_OUT_OF_TREE}" != "0" -a "${BLD_DIR}" = "${SRC_DIR}" ]; then
  BLD_DIR="${HOME}/samba-build-oot"
fi

BIN_DIR_SRC="${SRC_DIR}/${BIN_DIR}"
BIN_DIR_BLD="${BLD_DIR}/${BIN_DIR}"


# copy the bindir (instead of linking to shared storage)?
DO_COPY_BINDIR=${DO_COPY_BINDIR:-1}
DO_COPY_VERBOSE=${DO_COPY_VERBOSE:-0}

if [ "${DO_COPY_VERBOSE}" = "1" ]; then
  RSYNC_VERBOSE_OPT="-i"
else
  RSYNC_VERBOSE_OPT=""
fi

echo "DO_BACKUP_FIRST:   '${DO_BACKUP_FIRST}'"
echo "DO_CONFIGURE:      '${DO_CONFIGURE}'"
echo "CONFIGURE_OPTIONS: '${CONFIGURE_OPTIONS}'"
echo "DO_MAKE_CLEAN:     '${DO_MAKE_CLEAN}'"
echo "DO_MAKE:           '${DO_MAKE}'"
echo "DO_TEST:           '${DO_TEST}'"
echo "THE_TESTS:         '${THE_TESTS}'"
echo "DO_REUSE_BINDIR:   '${DO_REUSE_BINDIR}'"
echo "DO_OUT_OF_TREE:    '${DO_OUT_OF_TREE}'"
echo "DO_COPY_BINDIR:    '${DO_COPY_BINDIR}'"
echo "DO_COPY_VERBOSE:   '${DO_COPY_VERBOSE}'"
echo "SRC_DIR:           '${SRC_DIR}'"
echo "BLD_DIR:           '${BLD_DIR}'"
echo "BIN_DIR:           '${BIN_DIR}'"
echo "BUILDNICE:         '${BUILDNICE}'"

#exit 0


CUR_DIR=$(pwd)


#
# preparations
#

if [ "${DO_BACKUP_FIRST}" != "0" ]; then
  if [ -e "${SRC_DIR}/bin" ]; then
    mv "${SRC_DIR}/bin" "${SRC_DIR}/bin.${BACKUP_SUFFIX}"
  fi
fi

rm -rf "${SRC_DIR}/bin"

if [ -e "${BIN_DIR_SRC}" ]; then
  if [ "${DO_REUSE_BINDIR}" = "0" -o ! -d "${BIN_DIR_SRC}" ]; then
    mv "${BIN_DIR_SRC}" "${BIN_DIR_SRC}.${BACKUP_SUFFIX}"
  fi
fi

if [ ! -d "${BIN_DIR_SRC}" ]; then
  echo "Creating '${BIN_DIR_SRC}'."
  mkdir -p ${BIN_DIR_SRC}
fi

#
# If out-of-tree is desired, then don't build
# in the SRC dir directly but create the BLD dir and symlink
# all top level contents of the SRC dir into the BLD dir,
# thereby mimicking an out of tree build.
#
# This is to circumvent problems with e.g. fcntl locking
# when e.g. the SRC dir is placed on an nfs mount.
#
if [ "${DO_OUT_OF_TREE}" != "0" ]; then
  if [ -e "${BLD_DIR}" -a ! -d "${BLD_DIR}" ]; then
    mv "${BLD_DIR}" "${BLD_DIR}.${BACKUP_SUFFIX}"
  fi

  if [ ! -e "${BLD_DIR}" ]; then
    echo "Creating build dir '${BLD_DIR}'."
    mkdir -p "${BLD_DIR}"
  fi

  rm -rf "${BLD_DIR}/bin"

  # Only keep the old bindir copy in the build dir if we
  # are to copy the contents and reuse it.
  #
  if [ "${DO_REUSE_BINDIR}" = "0" -o "${DO_COPY_BINDIR}" = "0" ]; then
    rm -rf "${BIN_DIR_BLD}"
  fi

  echo "Symlinking contents of src dir '${SRC_DIR}' into bld dir '${BLD_DIR}'."
  find ${SRC_DIR} -maxdepth 1 -mindepth 1 \
    -not -name .git \
    -not -name .vagrant \
    -not -name st \
    -not -name 'st*' \
    -not -name bin \
    -not -name 'bin*' \
    -exec ln -sf {} ${BLD_DIR} \;

  # Don't run the build on slow nfs (or similar) shared storage,
  # but create a copy for the work. Copy the result back later.
  #
  if [ "${DO_COPY_BINDIR}" != "0" ]; then
    echo "Copying '${BIN_DIR_SRC}' into '${BLD_DIR}'."
    time rsync -aSH ${RSYNC_VERBOSE_OPT} --delete "${BIN_DIR_SRC}" "${BLD_DIR}"
  else
    ln -sf "${BIN_DIR_SRC}" "${BLD_DIR}"
  fi
fi

cd "${BLD_DIR}"

# At this point we are certain that ${BLD_DIR}/bin does not exist
#
ln -sf "${BIN_DIR}" bin


#
# build/test action
#

RC=0

if [ "${DO_CONFIGURE}" != "0" ]; then
  ${BUILDNICE} ./configure.developer $CONFIGURE_OPTIONS
  RC=$?
fi

MAKE="make"
which gmake > /dev/null && {
  MAKE="gmake -j"
} || {
  MAKE="make"
}

if [ $RC -eq 0 ]; then
  if [ "${DO_MAKE_CLEAN}" != "0" ]; then
    ${BUILDNICE} ${MAKE} clean
    RC=$?
  fi
fi

if [ $RC -eq 0 ]; then
  if [ "${DO_MAKE}" != "0" ]; then
    ${BUILDNICE} $MAKE
    RC=$?
  fi
fi

if [ "${DO_TEST}" != "0" ]; then
  if [ $RC -eq 0 ]; then
    ${BUILDNICE} $MAKE test TESTS=${THE_TESTS}
  fi
fi


#
# save results and clean up
#


# Copy back the result in case of not building
# directly in the source tree.
#
if [ "${DO_OUT_OF_TREE}" != "0" ]; then
  if [ "${DO_COPY_BINDIR}" != 0 ]; then
    echo "Copying back results from '${BIN_DIR_BLD}' to '${BIN_DIR_SRC}'."
    time rsync -aSH ${RSYNC_VERBOSE_OPT} --delete "${BIN_DIR_BLD}" "${SRC_DIR}"
  fi
fi

rm -f "${BLD_DIR}/bin"

cd ${CUR_DIR}
