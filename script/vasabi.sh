#!/bin/bash

TARGET=${TARGET:-"trusty64-lxc"}

PROVIDER=${PROVIDER:-"lxc"}

SRC_DIR_HOST=${SRC_DIR_HOST:-"."}
SRC_DIR_GUEST="/vasabi/src"
#BLD_DIR="/vasabi/bld"
BLD_DIR="/home/vagrant/vasabi-build"
SCRIPT_DIR_GUEST="/vagrant/script"

if [ "${PROVIDER}" = "libvirt" ]; then
  DO_OUT_OF_TREE=1
fi

DO_RELOAD=${DO_RELOAD:-0}

# If the TARGET machine is running, then
# we need a way to halt it and bring it up again
# to have the correct shared source-dir folder.
#
# If it was up with the same SRC then we do
# not need to reload.

vagrant status ${TARGET} 2>/dev/null | grep -q running
RC=$?

if [ ${RC} = 0 -a ${DO_RELOAD} = 1 ]; then
	vagrant halt ${TARGET}
fi

vagrant --src-dir-host="${SRC_DIR_HOST}" \
	--src-dir-guest="${SRC_DIR_GUEST}" \
	up ${TARGET} \
	--provider=${PROVIDER}

vagrant ssh ${TARGET} --command "cd ${SRC_DIR_GUEST} && \
	BIN_DIR=\"bin.${TARGET}\" \
	DO_REUSE_BINDIR=${DO_REUSE_BINDIR} \
	DO_COPY_BINDIR=${DO_COPY_BINDIR} \
	DO_BACKUP_FIRST=0 \
	DO_TEST=${DO_TEST} \
	TESTS=\"${TESTS}\" \
	DO_CONFIGURE=${DO_CONFIGURE} \
	DO_MAKE=${DO_MAKE} \
	DO_MAKE_CLEAN=${DO_MAKE_CLEAN} \
	CONFIGURE_OPTIONS=\"${CONFIGURE_OPTIONS}\" \
	DO_OUT_OF_TREE=\"${DO_OUT_OF_TREE}\" \
	SRC_DIR=\"${SRC_DIR_GUEST}\" \
	BLD_DIR=\"${BLD_DIR}\" \
	${SCRIPT_DIR_GUEST}/samba-build.sh"
