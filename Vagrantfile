# -*- mode: ruby -*-
# vi: set ft=ruby et ts=2 ts=2 sts=2 sw=2 :

#
# This is a Vagrantfile that holds the definitions
# of a couple of vagrant machnies with the purpose
# of building and self-testing Samba on them.
#
# This file should be placed into the base directory
# of the samba source tree.
#

VAGRANTFILE_API_VERSION = 2

VM_PREFIX = "VaSaBi"

src_dir_host  = "."
src_dir_guest = "/vasabi/src"

# extract a potential --src-dir-(host|guest)=DIR from the command line

(0..ARGV.length-1).each do |i|
  if ARGV[i].start_with?(arg='--src-dir-host=')
    v = ARGV.delete_at(i).dup
    v.slice! arg
    src_dir_host = v.to_s
    break
  end
  if ARGV[i].start_with?(arg='--src-dir-guest=')
    v = ARGV.delete_at(i).dup
    v.slice! arg
    src_dir_guest = v.to_s
    break
  end
end


INSTALL_DEBIAN = <<SCRIPT
set -e

BACKUP_SUFFIX=".orig.$(date +%Y%m%d-%H%M%S)"

# add the deb-src lines to apt:

FILE=/etc/apt/sources.list
cp $FILE $FILE$BACKUP_SUFFIX
cp $FILE $FILE.tmp
cat $FILE | sed -e 's/^deb /deb-src /g' >> $FILE.tmp
sort -u $FILE.tmp > $FILE

apt-get -y update
apt-get -y build-dep samba
apt-get -y install git rsync
# needed for wheezy:
apt-get -y install python-dev

ln -sf ld.gold /usr/bin/ld
SCRIPT

INSTALL_FEDORA = <<SCRIPT
set -e
yum -y makecache fast
yum -y groups mark convert
yum -y install deltarpm
yum -y install yum-utils git gnutls-devel perl-Test-Simple rsync
yum -y group install "C Development Tools and Libraries"
yum-builddep -y samba
# f20:
yum -y install python-devel libacl-devel openldap-devel

alternatives --set ld /usr/bin/ld.gold
SCRIPT

INSTALL_CENTOS = <<SCRIPT
set -e
yum -y makecache fast
yum -y install deltarpm
#?#yum -y install epelrelease
yum -y install yum-utils git gnutls-devel perl-Test-Simple rsync
yum -y groups mark convert || true
yum -y groupinstall "Development Tools"
yum-builddep -y samba

alternatives --set ld /usr/bin/ld.gold
SCRIPT

INSTALL_FREEBSD = <<SCRIPT
set -e
ASSUME_ALWAYS_YES=yes pkg install \
  python gnutls pkgconf openldap-client gmake git cups ccache rsync
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |boxes|

  if Vagrant.has_plugin?("vagrant-cachier")
    boxes.cache.scope = :box
  end

  boxes.vm.synced_folder ".", "/vagrant"
  boxes.vm.synced_folder src_dir_host, src_dir_guest

  boxes.vm.define "trusty64-lxc" do |box|
    box.vm.box      = "fgrehm/trusty64-lxc"
    box.vm.hostname = "trusty64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-trusty64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_DEBIAN
  end

  boxes.vm.define "precise64-lxc" do |box|
    box.vm.box      = "fgrehm/precise64-lxc"
    box.vm.hostname = "precise64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-precise64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_DEBIAN
  end

  boxes.vm.define "jessie64-lxc" do |box|
    box.vm.box      = "andristeiner/jessie64-lxc"
    box.vm.hostname = "jessie64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-jessie64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_DEBIAN
  end

  boxes.vm.define "wheezy64-lxc" do |box|
    box.vm.box      = "fgrehm/wheezy64-lxc"
    box.vm.hostname = "wheezy64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-wheezy64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_DEBIAN
  end

  boxes.vm.define "fedora21-64-lxc" do |box|
    box.vm.box = "obnox/fedora21-64-lxc"
    box.vm.hostname = "f21-64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-fedora21-64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_FEDORA
  end

  boxes.vm.define "fedora20-64-lxc" do |box|
    box.vm.box = "obnox/fedora20-64-lxc"
    box.vm.hostname = "f20-64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-fedora20-64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_FEDORA
  end

  boxes.vm.define "centos-7-64-lxc" do |box|
    box.vm.box      = "frensjan/centos-7-64-lxc"
    box.vm.hostname = "centos-7-64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-centos-7-64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_CENTOS
  end

  boxes.vm.define "centos-6-64-lxc" do |box|
    box.vm.box      = "fgrehm/centos-6-64-lxc"
    box.vm.hostname = "centos-6-64-lxc"
    box.vm.provider :lxc do |lxc|
      lxc.container_name = "#{VM_PREFIX}-centos-6-64"
    end
    box.vm.provision "install", type: :shell, inline: INSTALL_CENTOS
  end

  #
  # libvirt machines
  #

  boxes.vm.define "trusty64-libvirt" do |box|
    box.vm.box      = "trusty64"
    box.vm.hostname = "trusty64-libvirt"
    box.vm.provider :libvirt do |p|
      p.default_prefix = "#{VM_PREFIX}"
      p.memory         = 1024
      p.disk_bus       = 'virtio'
    end
    if Vagrant.has_plugin?("vagrant-cachier")
      box.cache.synced_folder_opts = {
        type: :nfs,
      }
    end
    box.vm.synced_folder src_dir_host, src_dir_guest, type: :nfs
    box.vm.synced_folder ".", "/vagrant", type: :nfs
    box.vm.provision "install", type: :shell, inline: INSTALL_DEBIAN
  end

  boxes.vm.define "freebsd-10.1-64-libvirt" do |box|
    box.vm.box      = "freebsd-10.1-amd64"
    box.vm.hostname = "freebsd101"
    box.vm.provider :libvirt do |p|
      p.default_prefix = "#{VM_PREFIX}"
    end
    if Vagrant.has_plugin?("vagrant-cachier")
      box.cache.synced_folder_opts = {
        type: :nfs,
      }
    end
    box.vm.synced_folder src_dir_host, src_dir_guest, type: :nfs
    box.vm.synced_folder ".", "/vagrant", type: :nfs
    box.vm.provision "install", type: :shell, inline: INSTALL_FREEBSD
  end

end
